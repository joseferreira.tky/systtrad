import backtrader as bt
import backtrader.feeds as btfeeds
import pyfolio as pf
import datetime
import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn.linear_model import LinearRegression

def filter_by_mktcap(mktcap, cutoff):
    # extract the tickers with market cap larger than the cutoff 
    # in the paper the cutoff is set to 1 billion
    # this is not really a good method to run over several years 
    # since more and more companies will satisfy this criteria
    # TODO: improve by using a top x percentile?
    # for now set to 10 billion which gives a universe of around 200-500 stocks between 2008-2019  
    large_cap = mktcap[mktcap["mkvalt"]>cutoff]
    large_cap_tickers = large_cap.tic.unique()
    return large_cap_tickers

# Load price/vol and market cap datasets for a given year
def load_datasets(year):
    data = pd.read_pickle(f"./data_{year}.pkl")
    # market cap as of the year before
    mktcap = pd.read_pickle(f"./mktcap_{year}.pkl")
    data = data.set_index("date")
    # ensure return data is numeric
    data["RET"] = pd.to_numeric(data["RET"], errors='coerce')
    # load eigen portfolios from the previous year
    eigen_portfolios = pd.read_pickle(f"./eigen_{year-1}.pkl")

    return data, mktcap, eigen_portfolios

def calculate_residuals(stock_returns, factor_returns, num_components):
    residuals = pd.DataFrame(columns = stock_returns.columns, index = stock_returns.index)
    betas = pd.DataFrame(columns = stock_returns.columns)
    ols = LinearRegression()
    for ticker in stock_returns.columns:
        ols.fit(factor_returns.iloc[:, 0:num_components-1], stock_returns[ticker])
        fitted_returns = ols.intercept_ + np.dot(factor_returns.iloc[:, 0:num_components-1], ols.coef_)
        residuals[ticker] = stock_returns[ticker] - fitted_returns
        betas[ticker] = ols.coef_
    return residuals.dropna(), betas.dropna()

def calculate_pca_factors(returns, pca_components):
    # standardize the return
    mean = returns.mean(axis = 0)
    std = returns.std(axis = 0)
    std_ret = (returns - mean)/std
    
    #PCA process
    pca = PCA(n_components = pca_components)
    pca.fit(std_ret)
    eigen_portfolios = pd.DataFrame(pca.components_)
    eigen_portfolios.columns = std.index
    eigen_portfolios = eigen_portfolios/std
    factor_returns = pd.DataFrame(np.dot(returns, eigen_portfolios.transpose()),index = returns.index)
    return pca, eigen_portfolios, factor_returns

#reference: 
#https://commoditymodels.files.wordpress.com/2010/02/
#estimating-the-parameters-of-a-mean-reverting-ornstein-uhlenbeck-process1.pdf
def calculate_s_score(residuals, mean_reversion_speed_thresh):
    m = pd.Series()
    sigma_eq = pd.Series()
    for ticker in residuals.columns:
        stock_res = residuals[ticker]
        x_k = stock_res.cumsum()
        x_lag = x_k.shift(1)
        x_lag[1] = 0
        
        #run OLS regression to find b ==> kappa
        ols = LinearRegression(fit_intercept=True)
        ols.fit(x_lag.values.reshape(-1,1), x_k)
        b = ols.coef_[0]
        kappa = -np.log(b)*252

        #select only the stocks with fast mean reversion i.e. speed larger than the threshold
        if kappa >= mean_reversion_speed_thresh:
            a = ols.intercept_
            m[ticker] = a/(1-b)
            residual = (x_k - ols.predict(x_lag.values.reshape(-1,1)))
            sigma_eq[ticker] = np.std(residual)*np.sqrt(1/(1-b*b) )
    m = m.dropna()
    m = m - m.mean()
    sigma_eq = sigma_eq.dropna()
    s_score = -m/sigma_eq
    return s_score

# Rebalance strategy sub-class
class RebalanceStrategy(bt.Strategy):

        
    def log(self, txt, dt=None):
            dt = dt or self.datas[0].datetime.date(0)
            print('%s, %s' % (dt.isoformat(), txt))

    # callable object that determines if there is a rebalance event in the date passed as parameter
    class IsBalanceDate(object):
        bal_dates = None
        def __call__(self, d):
            date_str = d.strftime("%Y-%m-%d")
            if date_str in bal_dates:
                return True
            else:
                return False
            
    def execute_target_orders(self, new_weights):
        sell_orders = {}
        buy_orders = {}
        
        for data in self.datas:
            if data._name in new_weights.index:
                current_size = self.getposition(data).size * data.close[0]
                new_target = new_weights[data._name]
                if current_size >= new_target:
                    sell_orders[data._name] = new_target
                else: 
                    buy_orders[data._name] = new_target
        # Sell first, then buy to avoid running out of cash
        for key, value in sell_orders.items():
            d = self.getdatabyname(key)
            self.order = self.order_target_value(data=d, target=value)
        for key, value in buy_orders.items():
            d = self.getdatabyname(key)
            self.order = self.order_target_value(data=d, target=value)
    
    def rebalance(self):
        raise NotImplementedError()
    
    def notify_timer(self, timer, when, *args, **kwargs):
        # Rebalance
        self.rebalance()
    
    def prenext(self):
        self.next()
       
    def notify_order(self, order):
        if order.status ==order.Completed:
            dt, dn = self.datetime.date(), order.data._name
            if self.debug: 
                self.log('{} Order {} Status {}:| Size:{} | Price:${}'.format(dn, order.ref, order.getstatusname(),
                                                                              order.size, order.executed.price))
        #else:
            #dt, dn = self.datetime.date(), order.data._name
            #if self.debug: 
                #self.log('{} Order {} Status {}'.format(dn, order.ref, order.getstatusname()))
       
    def next(self):
        # show current status
        if self.position and self.debug: self.log(f"Current portfolio value: {self.broker.get_value()}")            
            
# Generic method for presenting backtesting results using pyfolio
# Requires to have pyfolio observer added, the input parameters are the cerebro instance and the results of its run method
def present_results(cerebro, results):
    print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())
    strat = results[0]
    pyfoliozer = strat.analyzers.getbyname('pyfolio')
    returns, positions, transactions, gross_lev = pyfoliozer.get_pf_items()
    pf.create_full_tear_sheet(returns)